package net.evendanan.pushingpixels;

/**
 * Created by hamburg on 11/6/13.
 */
public interface Passengerable {
    void setItemExpandExtraData(float originateViewCenterX, float originateViewCenterY,
                                float originateViewWidthScale, float originateViewHeightScale);
}
