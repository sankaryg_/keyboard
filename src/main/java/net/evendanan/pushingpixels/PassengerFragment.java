package net.evendanan.pushingpixels;

import android.support.v4.app.Fragment;
import android.view.animation.Animation;

public abstract class PassengerFragment extends Fragment implements Passengerable {

    private static final String TAG = "PassengerFragment";

    @Override
    public void setItemExpandExtraData(float originateViewCenterX, float originateViewCenterY,
                                       float originateViewWidthScale, float originateViewHeightScale) {
        PassengerFragmentSupport.setItemExpandExtraData(this,
                originateViewCenterX, originateViewCenterY,
                originateViewWidthScale, originateViewHeightScale);
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation customAnimation = PassengerFragmentSupport.onCreateAnimation(this, transit, enter, nextAnim);
        if (customAnimation == null)
            return super.onCreateAnimation(transit, enter, nextAnim);
        else
            return customAnimation;
    }
}
