package com.hamburg.android.anysoftkeyboard;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.Window;
import android.view.WindowManager;


public class SplashScreen  extends Activity{
	SharedPreferences sharedPreference;
	 //stopping splash screen starting home activity.  
    private static final int STOPSPLASH = 0;  
    //time duration in millisecond for which your splash screen should visible to  
      //user. here i have taken half second  
    private static final long SPLASHTIME = 2000; 
  //handler for splash screen  
    private Handler splashHandler = new Handler() {  
         @Override  
         public void handleMessage(Message msg) {  
              switch (msg.what) {  
                case STOPSPLASH:  
                    //Generating and Starting new intent on splash time out
                	Intent idea = new Intent(getApplicationContext(),MainActivity.class);
           			startActivity(idea);
           			SplashScreen.this.finish();
           		    break;
              }  
              super.handleMessage(msg);  
         }  
    };  
    boolean analyticsOn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
		  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                  WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.splashscreen);
        final SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(this);
        analyticsOn = sp.getBoolean("analytics",true);
        if (analyticsOn) {
            AnyApplication.getInstance().getGoogleAnalyticsTracker().enableAutoActivityTracking(true);
            AnyApplication.getInstance().trackEvent("HH-Emojis", "App Open", "", "start", "Session");
        }else {
            AnyApplication.getInstance().getGoogleAnalyticsTracker().enableAutoActivityTracking(false);
        }
		//Generating message and sending it to splash handle
		// Creating Database before the app get starts.
        Message msg = new Message();  
        msg.what = STOPSPLASH; 
        splashHandler.sendMessageDelayed(msg, SPLASHTIME);  
	}
	
}
