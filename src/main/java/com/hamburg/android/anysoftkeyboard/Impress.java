package com.hamburg.android.anysoftkeyboard;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.TextView;

public class Impress extends BaseActivity implements View.OnClickListener {
    Toolbar toolbar;
    TextView text2,email,text3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_impress);
        toolbar = (Toolbar) findViewById(R.id.imptools); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);

        TextView text=(TextView) findViewById(R.id.imptitle);
        TextView im_url=(TextView)findViewById(R.id.impurl);
        TextView text1=(TextView) findViewById(R.id.imptext1);
        TextView title1=(TextView) findViewById(R.id.tit1);
        TextView title2=(TextView) findViewById(R.id.tit2);
        email=(TextView)findViewById(R.id.email);
        text2=(TextView)findViewById(R.id.imptext2);
        text3=(TextView)findViewById(R.id.text3);

        text2.setTypeface(Typeface.createFromAsset(getAssets(), "ClanPro-Book.ttf"));
        text.setTypeface(Typeface.createFromAsset(getAssets(), "ClanPro-Book.ttf"));
        text1.setTypeface(Typeface.createFromAsset(getAssets(), "ClanPro-Book.ttf"));
        title1.setTypeface(Typeface.createFromAsset(getAssets(), "ClanPro-Book.ttf"));
        title2.setTypeface(Typeface.createFromAsset(getAssets(), "ClanPro-Book.ttf"));
        im_url.setTypeface(Typeface.createFromAsset(getAssets(), "ClanPro-Book.ttf"));
        im_url.setMovementMethod(LinkMovementMethod.getInstance());

        SpannableString content = new SpannableString("Content");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        text1.setText(content);
        email.setText(R.string.email);
        text2.setText(R.string.strings2);
        text3.setText(R.string.string3);
        im_url.setText(R.string.impurl);
        //text1.setText(R.string.kontakt);

        String fullText = getString(R.string.kontakt);
        final String justClickHereText = getString(R.string.email1);
        SpannableStringBuilder sb = new SpannableStringBuilder(fullText);
        // Get the index of "click here" string.
        int start = fullText.indexOf(justClickHereText);
        int length = justClickHereText.length();
        if (start == -1) {
            //this could happen when the localization is not correct
            start = 0;
            length = fullText.length();
        }
        ClickableSpan csp = new ClickableSpan() {
            @Override
            public void onClick(View v) {
                Intent emailintent = new Intent(Intent.ACTION_SEND);
                emailintent.setType("plain/text");
                emailintent.putExtra(Intent.EXTRA_EMAIL,new String[] {justClickHereText });
                emailintent.putExtra(Intent.EXTRA_SUBJECT, "");
                emailintent.putExtra(Intent.EXTRA_TEXT,"");
                startActivity(Intent.createChooser(emailintent, "Send mail..."));
            }
        };
        sb.setSpan(csp, start, start + length, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        text1.setMovementMethod(LinkMovementMethod.getInstance());
        text1.setText(sb);


        email.setOnClickListener(this);
        //text1.setOnClickListener(this);
    }

   public void menu_impress_click(View view){
       Intent intent=new Intent(Impress.this,MainActivity.class);
       intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
       intent.putExtra("menu",true);
       startActivity(intent);
       overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
   }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(Impress.this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void onbackimpress_Arrow(View view){
    Intent intent=new Intent(Impress.this,MainActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
}


    @Override
    public void onClick(View v) {
        Intent emailintent = new Intent(Intent.ACTION_SEND);
        emailintent.setType("plain/text");
        emailintent.putExtra(Intent.EXTRA_EMAIL,new String[] {"haspa@haspa.de" });
        emailintent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailintent.putExtra(Intent.EXTRA_TEXT,"");
        startActivity(Intent.createChooser(emailintent, "Send mail..."));

    }
}
