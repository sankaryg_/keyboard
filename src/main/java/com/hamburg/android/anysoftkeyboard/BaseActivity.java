package com.hamburg.android.anysoftkeyboard;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;

public class BaseActivity extends AppCompatActivity {
	private BaseActivity _activity;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		// hide statusbar of Android
		// could also be done later
		/*
		 * this.getWindow().setFlags(
		 * WindowManager.LayoutParams.FLAG_FULLSCREEN,
		 * WindowManager.LayoutParams.FLAG_FULLSCREEN );
		 * this.getWindow().setFlags(
		 * WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
		 * WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );
		 */
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		_activity = this;

	}

	public void setupUI(View view) {

		// Set up touch listener for non-text box views to hide keyboard.
		if (!(view instanceof EditText)) {

			view.setOnTouchListener(new OnTouchListener() {

				public boolean onTouch(View v, MotionEvent event) {
					hideSoftKeyboard(BaseActivity.this);
					return false;
				}

			});
		}

		// If a layout container, iterate over children and seed recursion.
		if (view instanceof ViewGroup) {

			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

				View innerView = ((ViewGroup) view).getChildAt(i);

				setupUI(innerView);
			}
		}
	}

	public void hideSoftKeyboard(Activity activity) {
		try {
			if(activity!=null){
			InputMethodManager inputMethodManager = (InputMethodManager) activity
					.getSystemService(Activity.INPUT_METHOD_SERVICE);
			if(activity.getCurrentFocus()!=null)
			inputMethodManager.hideSoftInputFromWindow(activity
					.getCurrentFocus().getWindowToken(), 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void launchActivity(Class<?> clazz) {
		Intent intent = new Intent(_activity, clazz);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		_activity.startActivity(intent);

	}

	public enum AnimationType {
		ALPHA,RIGHT,LEFT;
	}
	public void showAlphaAnimation(int id,AnimationType animationType) {
		int animId = R.anim.alpha;
		switch (animationType){
			case ALPHA:
				animId = R.anim.alpha;
				break;
			case RIGHT:
				animId = R.anim.slide_in_right;
				break;
			case LEFT:
				animId = R.anim.slide_out_left;
				break;

		}
		Animation anim = AnimationUtils.loadAnimation(_activity, animId);
		if (anim == null)
			return;
		anim.reset();
		if (_activity.findViewById(id) != null) {
			if (_activity.findViewById(id) instanceof LinearLayout) {
				_activity.findViewById(id).clearAnimation();
				_activity.findViewById(id).startAnimation(anim);
			}
		}
	}

	
	public void updateDateTimeValue(String string, Calendar calendar_fragment,
			int id) {

	}
	
	/*
	 * Sets the font on all TextViews in the ViewGroup. Searches recursively for
	 * all inner ViewGroups as well. Just add a check for any other views you
	 * want to set as well (EditText, etc.)
	 */
	public void setFont(ViewGroup group, Typeface font) {
		int count = group.getChildCount();
		View v;
		for (int i = 0; i < count; i++) {
			v = group.getChildAt(i);
			if (v instanceof TextView || v instanceof Button /* etc. */)
				((TextView) v).setTypeface(font);
			else if (v instanceof ViewGroup)
				setFont((ViewGroup) v, font);
		}
	}
	
	public void displayFrameworkBugMessageAndExit(String msg) {
		/*AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
		builder.setTitle(getString(R.string.app_name));
		builder.setMessage(msg);
		builder.setPositiveButton(getResources().getString(R.string.yes),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						arg0.dismiss();
						launchActivity(PropertyAnalysisHomeActivity.class);
					}
				});
		builder.setNegativeButton(getResources().getString(R.string.no),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						arg0.dismiss();

					}
				});
		builder.show();*/
	}
}
