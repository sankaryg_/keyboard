package com.hamburg.android.anysoftkeyboard;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.anysoftkeyboard.ui.settings.setup.SetupSupport;

public class MainActivity extends BaseActivity implements View.OnClickListener,View.OnTouchListener{

    private ImageView menuList;
    private LinearLayout homeLayout;
    private LinearLayout menuLayout;
    private Button homeBtn;
    private Button urlBtn;
    private Button dateBtn;
    private Button impressBtn;
    private Button enableHumburg;
    private Button enableKeyboard;
    private ImageButton whatsup;
    private ImageButton wechart;
    private ImageButton messanger;
    private PackageManager pm;

    private Context mBaseContext = null;
    private Intent mReLaunchTaskIntent = null;
    private Context mAppContext;


    private static final int KEY_MESSAGE_UNREGISTER_LISTENER = 447;
    private static final int KEY_MESSAGE_RETURN_TO_APP = 446;
    private static final int NONE = 0;
    private static final int PICKING = 1;
    private static final int CHOSEN = 2;
    private int mState;

    private Handler mGetBackHereHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case KEY_MESSAGE_RETURN_TO_APP:
                    if (mReLaunchTaskIntent != null && mBaseContext != null) {
                        //mBaseContext.startActivity(mReLaunchTaskIntent);
                        mReLaunchTaskIntent = null;
                    }
                    break;
                case KEY_MESSAGE_UNREGISTER_LISTENER:
                    unregisterSettingsObserverNow();
                    break;
            }
        }
    };
    private InputMethodChangeReceiver mReceiver = new InputMethodChangeReceiver();

    public class InputMethodChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_INPUT_METHOD_CHANGED)) {
                if (SetupSupport.isThisKeyboardSetAsDefaultIME(MainActivity.this)) {
                    enableKeyboard.setClickable(false);
                    enableKeyboard.setBackgroundResource(R.drawable.shape_bg);
                    enableKeyboard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_icon,0,0,0);
                    unregisterReceiver(this);
                }
            }
        }
    }

    private final ContentObserver mSecureSettingsChanged = new ContentObserver(null) {
        @Override
        public boolean deliverSelfNotifications() {
            return false;
        }

        @Override
        public void onChange(boolean selfChange) {
                if (SetupSupport.isThisKeyboardEnabled(MainActivity.this)) {
                    //should we return to this task?
                    //this happens when the user is asked to enable AnySoftKeyboard, which is done on a different UI activity (outside of my App).
                    mGetBackHereHandler.removeMessages(KEY_MESSAGE_RETURN_TO_APP);
                    mGetBackHereHandler.sendMessageDelayed(mGetBackHereHandler.obtainMessage(KEY_MESSAGE_RETURN_TO_APP), 50/*enough for the user to see what happened.*/);
                }
            }

    };

    private void unregisterSettingsObserverNow() {
        mGetBackHereHandler.removeMessages(KEY_MESSAGE_UNREGISTER_LISTENER);
        if (mAppContext != null) {
            mAppContext.getContentResolver().unregisterContentObserver(mSecureSettingsChanged);
            mAppContext = null;
        }
    }
    private boolean showMenu = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mState = NONE;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            showMenu = bundle.getBoolean("menu");
        }
        mBaseContext = this;
        mReLaunchTaskIntent = new Intent(this,MainActivity.class);
        menuList = (ImageView)findViewById(R.id.mainmenu);
        homeLayout = (LinearLayout)findViewById(R.id.hom_layout);
        menuLayout = (LinearLayout)findViewById(R.id.menu_layout);
        homeBtn = (Button)findViewById(R.id.haspasa);
        urlBtn = (Button)findViewById(R.id.url);
        dateBtn = (Button)findViewById(R.id.date);
        impressBtn = (Button)findViewById(R.id.impress);
        enableHumburg = (Button)findViewById(R.id.keyboard1);
        enableKeyboard = (Button)findViewById(R.id.keyboard2);
        enableHumburg.setOnClickListener(this);
        enableKeyboard.setOnClickListener(this);
        homeBtn.setOnClickListener(this);
        urlBtn.setOnClickListener(this);
        dateBtn.setOnClickListener(this);
        impressBtn.setOnClickListener(this);
        menuList.setOnClickListener(this);
        homeLayout.setVisibility(View.VISIBLE);
        pm=getPackageManager();
        whatsup=(ImageButton)findViewById(R.id.whatsup);
        //whatsup.setOnTouchListener(this);
        wechart=(ImageButton)findViewById(R.id.wechat);
        //wechart.setOnTouchListener(this);
        messanger=(ImageButton)findViewById(R.id.messanger);
        //messanger.setOnTouchListener(this);
        whatsup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");
                    String text = "YOUR TEXT HERE";
                    PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                    //Check if package exists or not. If not then code
                    //in catch block will be called
                    waIntent.setPackage("com.whatsapp");
                    waIntent.putExtra(Intent.EXTRA_TEXT, text);
                    startActivity(Intent.createChooser(waIntent, "Share with"));

                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        wechart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");
                    String text = "YOUR TEXT HERE";
                    PackageInfo info = pm.getPackageInfo("com.tencent.mm", PackageManager.GET_META_DATA);
                    //Check if package exists or not. If not then code
                    //in catch block will be called
                    waIntent.setPackage("com.tencent.mm");
                    waIntent.putExtra(Intent.EXTRA_TEXT, text);
                    startActivity(Intent.createChooser(waIntent, "Share with"));

                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this,R.string.app_does_not_exist,Toast.LENGTH_SHORT).show();
                }
            }
        });
        messanger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");
                    String text = "YOUR TEXT HERE";
                    PackageInfo info = pm.getPackageInfo("com.facebook.orca", PackageManager.GET_META_DATA);
                    //Check if package exists or not. If not then code
                    //in catch block will be called
                    waIntent.setPackage("com.facebook.orca");
                    waIntent.putExtra(Intent.EXTRA_TEXT, text);
                    startActivity(Intent.createChooser(waIntent, "Share with"));

                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

            }
        });
        enableHumburg.setCompoundDrawablesWithIntrinsicBounds(R.drawable.android_1, 0, 0, 0);
        enableKeyboard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.android_2,0,0,0);
        enableHumburg.setClickable(true);
        enableKeyboard.setClickable(true);
        if (SetupSupport.isThisKeyboardEnabled(this)) {
            enableHumburg.setClickable(false);
            enableHumburg.setBackgroundResource(R.drawable.shape_bg);
            enableHumburg.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_icon,0,0,0);
            if (SetupSupport.isThisKeyboardSetAsDefaultIME(this)) {
                enableKeyboard.setClickable(false);
                enableKeyboard.setBackgroundResource(R.drawable.shape_bg);
                enableKeyboard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_icon,0,0,0);
            }
        }


        if (showMenu){
            menuLayout.setVisibility(View.VISIBLE);
            homeLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(mState == PICKING) {
            mState = CHOSEN;
        }
        else if(mState == CHOSEN) {
            if (SetupSupport.isThisKeyboardSetAsDefaultIME(this)) {
                enableKeyboard.setClickable(false);
                enableKeyboard.setBackgroundResource(R.drawable.shape_bg);
                enableKeyboard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_icon,0,0,0);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v==menuList){
            if (homeLayout.getVisibility()==View.VISIBLE){
                homeLayout.scrollTo(0, 0);
                menuLayout.setVisibility(View.VISIBLE);
                //showAlphaAnimation(homeLayout.getId(),AnimationType.LEFT);
                homeLayout.setVisibility(View.GONE);
            }else {
                menuLayout.scrollTo(0, 0);
                //showAlphaAnimation(menuLayout.getId(),AnimationType.LEFT);
                menuLayout.setVisibility(View.GONE);
                homeLayout.setVisibility(View.VISIBLE);
            }
        }else if (v==homeBtn){
            menuLayout.scrollTo(0, 0);
            //showAlphaAnimation(menuLayout.getId(),AnimationType.LEFT);
            menuLayout.setVisibility(View.GONE);
            homeLayout.setVisibility(View.VISIBLE);
        }else if (v==urlBtn){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://haspa-persoenlich.de/"));
            startActivity(browserIntent);
        }else if (v==dateBtn){

            Intent intent3=new Intent(MainActivity.this,Last_Class.class);
            intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent3);
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        }else if (v== impressBtn){
            Intent intent2=new Intent(MainActivity.this,Impress.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent2);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }else if (v==enableHumburg){
//registering for changes, so I'll know to come back here.
            mAppContext = getApplicationContext();
            mAppContext.getContentResolver().registerContentObserver(Settings.Secure.CONTENT_URI, true, mSecureSettingsChanged);
            //but I don't want to listen for changes for ever!
            //If the user is taking too long to change one checkbox, I say forget about it.
            mGetBackHereHandler.removeMessages(KEY_MESSAGE_UNREGISTER_LISTENER);
            mGetBackHereHandler.sendMessageDelayed(mGetBackHereHandler.obtainMessage(KEY_MESSAGE_UNREGISTER_LISTENER),
                    45*1000/*45 seconds to change a checkbox is enough. After that, I wont listen to changes anymore.*/);
            Intent startSettings = new Intent(Settings.ACTION_INPUT_METHOD_SETTINGS);
            startSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            try {
                mAppContext.startActivity(startSettings);
            } catch (ActivityNotFoundException notFoundEx) {
                //weird.. the device does not have the IME setting activity. Nook?
               // Toast.makeText(mAppContext, R.string.setup_wizard_step_one_action_error_no_settings_activity, Toast.LENGTH_LONG).show();
            }
        }else if (v==enableKeyboard){
            InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            mgr.showInputMethodPicker();
            mState = PICKING;
            IntentFilter filter = new IntentFilter(Intent.ACTION_INPUT_METHOD_CHANGED);
            registerReceiver(mReceiver, filter);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {


              /*  switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        //overlay is black with transparency of 0x77 (119)
                        view.getDrawable().setColorFilter(Color.DKGRAY, PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        //clear the overlay
                        view.getDrawable().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }*/

                return true;


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SetupSupport.isThisKeyboardEnabled(this)) {
            enableHumburg.setClickable(false);
            enableHumburg.setBackgroundResource(R.drawable.shape_bg);
            enableHumburg.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_icon,0,0,0);
            if (SetupSupport.isThisKeyboardSetAsDefaultIME(this)) {
                enableKeyboard.setClickable(false);
                enableKeyboard.setBackgroundResource(R.drawable.shape_bg);
                enableKeyboard.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick_icon,0,0,0);
            }
        }
    }
}
