package com.hamburg.android.anysoftkeyboard;


import android.app.Application;
import android.content.ComponentName;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.preference.PreferenceManager;

import com.anysoftkeyboard.Configuration;
import com.anysoftkeyboard.ConfigurationImpl;
import com.anysoftkeyboard.backup.CloudBackupRequester;
import com.anysoftkeyboard.backup.CloudBackupRequesterDiagram;
import com.anysoftkeyboard.devicespecific.DeviceSpecific;
import com.anysoftkeyboard.devicespecific.StrictModeAble;
import com.anysoftkeyboard.utils.Log;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;

import net.evendanan.frankenrobot.Diagram;
import net.evendanan.frankenrobot.FrankenRobot;
import net.evendanan.frankenrobot.Lab;

import java.lang.reflect.Field;


public class AnyApplication extends Application implements OnSharedPreferenceChangeListener {

    private static final String TAG = "ASK_APP";
    private static Configuration msConfig;
    private static FrankenRobot msFrank;
    private static DeviceSpecific msDeviceSpecific;
    private static CloudBackupRequester msCloudBackupRequester;

    private static final String DEFAULT_NORMAL_FONT_FILENAME = "ClanPro-Book.ttf";
    private void setDefaultFont() {

        try {
            final Typeface bold = Typeface.createFromAsset(getAssets(), DEFAULT_NORMAL_FONT_FILENAME);//DEFAULT_BOLD_FONT_FILENAME);
            final Typeface italic = Typeface.createFromAsset(getAssets(), DEFAULT_NORMAL_FONT_FILENAME);//DEFAULT_ITALIC_FONT_FILENAME);
            final Typeface boldItalic = Typeface.createFromAsset(getAssets(), DEFAULT_NORMAL_FONT_FILENAME);//DEFAULT_BOLD_ITALIC_FONT_FILENAME);
            final Typeface regular = Typeface.createFromAsset(getAssets(),DEFAULT_NORMAL_FONT_FILENAME);

            Field DEFAULT = Typeface.class.getDeclaredField("DEFAULT");
            DEFAULT.setAccessible(true);
            DEFAULT.set(null, regular);

            Field DEFAULT_BOLD = Typeface.class.getDeclaredField("DEFAULT_BOLD");
            DEFAULT_BOLD.setAccessible(true);
            DEFAULT_BOLD.set(null, bold);

            Field sDefaults = Typeface.class.getDeclaredField("sDefaults");
            sDefaults.setAccessible(true);
            sDefaults.set(null, new Typeface[]{
                    regular, bold, italic, boldItalic
            });

        } catch (NoSuchFieldException e) {
            logFontError(e);
        } catch (IllegalAccessException e) {
            logFontError(e);
        } catch (Throwable e) {
            //cannot crash app if there is a failure with overriding the default font!
            logFontError(e);
        }
    }
    private void logFontError(Throwable e) {
        android.util.Log.e("font_override", "Error overriding font", e);
    }



    @Override
    public void onCreate() {
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler(new ChewbaccaUncaughtExceptionHandler(
                getBaseContext(), null));
        Log.d(TAG, "** Starting application in DEBUG mode.");
        mInstance = this;

        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
        msFrank = Lab.build(getApplicationContext(), R.array.frankenrobot_interfaces_mapping);
        if (BuildConfig.DEBUG) {
            StrictModeAble strictMode = msFrank.embody(StrictModeAble.class);
            if (strictMode != null)//it should be created only in the API18.
                strictMode.setupStrictMode();
        }
        setDefaultFont();
        msConfig = new ConfigurationImpl(this);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        sp.registerOnSharedPreferenceChangeListener(this);


        msDeviceSpecific = msFrank.embody(new Diagram<DeviceSpecific>() {
        });
        Log.i(TAG, "Loaded DeviceSpecific " + msDeviceSpecific.getApiLevel() + " concrete class " + msDeviceSpecific.getClass().getName());

        msCloudBackupRequester = msFrank.embody(new CloudBackupRequesterDiagram(getApplicationContext()));

        //TutorialsProvider.showDragonsIfNeeded(getApplicationContext());


    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        ((ConfigurationImpl) msConfig).onSharedPreferenceChanged(sharedPreferences, key);
        //should we disable the Settings App? com.hamburg.android.anysoftkeyboard.LauncherSettingsActivity
        if (key.equals(getString(R.string.settings_key_show_settings_app))) {
            PackageManager pm = getPackageManager();
            boolean showApp = sharedPreferences.getBoolean(key, getResources().getBoolean(R.bool.settings_default_show_settings_app));
            pm.setComponentEnabledSetting(new ComponentName(getApplicationContext(), com.hamburg.android.anysoftkeyboard.LauncherSettingsActivity.class),
                    showApp ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);
        }
    }


    public static Configuration getConfig() {
        return msConfig;
    }

    public static DeviceSpecific getDeviceSpecific() {
        return msDeviceSpecific;
    }

    public static void requestBackupToCloud() {
        if (msCloudBackupRequester != null)
            msCloudBackupRequester.notifyBackupManager();
    }

    public static FrankenRobot getFrankenRobot() {
        return msFrank;
    }




    private static AnyApplication mInstance;



    public static synchronized AnyApplication getInstance() {
        return mInstance;
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(screenName);

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                            .setDescription(
                                    new StandardExceptionParser(this, null)
                                            .getDescription(Thread.currentThread().getName(), e))
                            .setFatal(false)
                            .build()
            );
        }
    }

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public void trackEvent(String category, String action, String label,String paramName,String paramValue) {
        Tracker t = getGoogleAnalyticsTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label).set(paramName,paramValue).build());
    }
}
