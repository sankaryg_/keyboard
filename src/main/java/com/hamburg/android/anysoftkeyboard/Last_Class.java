package com.hamburg.android.anysoftkeyboard;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

public class Last_Class extends BaseActivity {

    Toolbar toolbar;
    private Switch analyticsToggle;
    private boolean analyticsOn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last__class);
        toolbar = (Toolbar) findViewById(R.id.lasttools); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);
        analyticsToggle = (Switch)findViewById(R.id.toggleButton);
        TextView title1=(TextView) findViewById(R.id.tit1);
        TextView last=(TextView) findViewById(R.id.lasttitle);
        TextView title2=(TextView) findViewById(R.id.tit2);
        TextView text1 = (TextView) findViewById(R.id.lasttext);
        TextView text2=(TextView)findViewById(R.id.downtext);
        TextView text= (TextView) findViewById(R.id.toggltext);
        title1.setTypeface(Typeface.createFromAsset(getAssets(), "ClanPro-Book.ttf"));
        title2.setTypeface(Typeface.createFromAsset(getAssets(), "ClanPro-Book.ttf"));
        last.setTypeface(Typeface.createFromAsset(getAssets(), "ClanPro-Book.ttf"));
        text1.setText(getString(R.string.datenschutz));
        final SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(this);
        analyticsOn = sp.getBoolean("analytics",true);
        text2.setTypeface(Typeface.createFromAsset(getAssets(), "ClanPro-Book.ttf"));
        text.setTypeface(Typeface.createFromAsset(getAssets(), "ClanPro-Book.ttf"));
        text1.setTypeface(Typeface.createFromAsset(getAssets(), "ClanPro-Book.ttf"));
        analyticsToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(analyticsToggle.isChecked()){
                analyticsOn = false;
                    sp.edit().putBoolean("analytics",false).apply();
                    AnyApplication.getInstance().getGoogleAnalyticsTracker().enableAutoActivityTracking(false);
                }
                else {
                analyticsOn = true;
                    sp.edit().putBoolean("analytics",true).apply();
                    AnyApplication.getInstance().getGoogleAnalyticsTracker().enableAutoActivityTracking(true);
                }
            }
        });
        if (analyticsOn){
            analyticsToggle.setChecked(false);
            AnyApplication.getInstance().getGoogleAnalyticsTracker().enableAutoActivityTracking(true);
        }else {
            analyticsToggle.setChecked(true);
            AnyApplication.getInstance().getGoogleAnalyticsTracker().enableAutoActivityTracking(false);
        }
    }

   public void onbacklast_Arrow(View view)
   {
       Intent intent=new Intent(Last_Class.this,MainActivity.class);
       intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
       startActivity(intent);
       overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
   }
    public void menu_last_click(View view){
        Intent intent=new Intent(Last_Class.this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("menu",true);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(Last_Class.this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
