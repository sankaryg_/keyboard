package com.anysoftkeyboard.devicespecific;

public interface StrictModeAble {
    void setupStrictMode();
}