

package com.anysoftkeyboard.devicespecific;

import android.annotation.TargetApi;
import android.content.Context;

@TargetApi(19)
public class AskV19GestureDetector extends AskV8GestureDetector {
    public AskV19GestureDetector(Context context, AskOnGestureListener listener) {
        super(context, listener);
	    //this behavior is not good for ASK. See https://github.com/AnySoftKeyboard/AnySoftKeyboard/issues/332
	    mScaleGestureDetector.setQuickScaleEnabled(false);
    }
}
