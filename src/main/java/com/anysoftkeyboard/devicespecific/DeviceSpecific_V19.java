

package com.anysoftkeyboard.devicespecific;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.GestureDetector;

@TargetApi(19)
public class DeviceSpecific_V19 extends DeviceSpecific_V11 {
	@Override
	public String getApiLevel() {
		return "DeviceSpecific_V19";
	}

	@Override
	public GestureDetector createGestureDetector(Context appContext, AskOnGestureListener listener) {
		return new AskV19GestureDetector(appContext, listener);
	}
}
