
package com.anysoftkeyboard;

import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

public interface Configuration {

    boolean alwaysUseFallBackUserDictionary();

    boolean hasNotificationAnimated(String notificationKey);

    void setNotificationAnimated(String notificationKey);

    void setNotificationClicked(String notificationKey);

    boolean hasNotificationClicked(String notificationKey);

    public static enum AnimationsLevel {
        Full,
        Some,
        None
    }

    String getDomainText();

    //String getChangeLayoutKeysSize();

    boolean getShowKeyPreview();

    boolean getShowKeyboardNameText();

    boolean getShowHintTextOnKeys();

    boolean getUseCustomHintAlign();

    int getCustomHintAlign();

    int getCustomHintVAlign();

    AnimationsLevel getAnimationsLevel();

    boolean getSwitchKeyboardOnSpace();

    boolean getUseFullScreenInputInLandscape();

    boolean getUseFullScreenInputInPortrait();

    boolean getUseRepeatingKeys();

    float getKeysHeightFactorInPortrait();

    float getKeysHeightFactorInLandscape();

    boolean getInsertSpaceAfterCandidatePick();

    int getGestureSwipeUpKeyCode(boolean fromSpaceBar);

    int getGestureSwipeDownKeyCode();

    int getGestureSwipeLeftKeyCode(boolean fromSpaceBar, boolean withTwoFingers);

    int getGestureSwipeRightKeyCode(boolean fromSpaceBar, boolean withTwoFingers);

    int getGesturePinchKeyCode();

    int getGestureSeparateKeyCode();

    boolean getActionKeyInvisibleWhenRequested();

    int getDeviceOrientation();

    //String getRtlWorkaroundConfiguration();

    boolean isDoubleSpaceChangesToPeriod();

    boolean shouldShowPopupForLanguageSwitch();

    boolean hideSoftKeyboardWhenPhysicalKeyPressed();

    boolean getShowTipsNotification();

    void setShowTipsNotification(boolean show);

    boolean use16KeysSymbolsKeyboards();

    boolean useBackword();

    boolean getCycleOverAllSymbols();

    boolean useVolumeKeyForLeftRight();

    boolean useCameraKeyForBackspaceBackword();

    boolean useContactsDictionary();

    int getAutoDictionaryInsertionThreshold();

    boolean isStickyExtensionKeyboard();

    boolean drawExtensionKeyboardAboveMainKeyboard();

    int getSwipeVelocityThreshold();

    int getSwipeDistanceThreshold();

    int getLongPressTimeout();

    int getMultiTapTimeout();

    boolean workaround_alwaysUseDrawText();

    String getInitialKeyboardCondenseState();

    boolean useChewbaccaNotifications();

    boolean showKeyPreviewAboveKey();

    void addChangedListener(OnSharedPreferenceChangeListener listener);

    void removeChangedListener(OnSharedPreferenceChangeListener listener);

    boolean shouldswapPunctuationAndSpace();

    int getCurrentAppVersion();

    int getFirstAppVersionInstalled();

    long getFirstTimeAppInstalled();

    long getTimeCurrentVersionInstalled();
}
