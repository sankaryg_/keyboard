package com.anysoftkeyboard.utils;

public class ModifierKeyState {
    private static final int RELEASING = 0;
    private static final int PRESSING = 1;
    private static final int MOMENTARY = 2;

    private int mState = RELEASING;

    public void onPress() {
        mState = PRESSING;
    }

    public void onRelease() {
        mState = RELEASING;
    }

    public void reset() {
        mState = RELEASING;
    }

    public void onOtherKeyPressed() {
        if (mState == PRESSING)
            mState = MOMENTARY;
    }

    public boolean isMomentary() {
        return mState == MOMENTARY;
    }
}
