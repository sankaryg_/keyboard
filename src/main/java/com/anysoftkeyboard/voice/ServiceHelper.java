package com.anysoftkeyboard.voice;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.speech.RecognizerIntent;

/**
 * Service helper that connects the IME with the activity that triggers the recognition
 * and that will receive the recognition result.
 */
public class ServiceHelper extends Service {

    private static final String TAG = "ServiceHelper";

    private final IBinder mBinder = new ServiceHelperBinder();

    private Callback mCallback;

    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }

    public void startRecognition(String languageLocale, Callback callback) {
        mCallback = callback;
        Intent intent = new Intent(this, ActivityHelper.class);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, languageLocale);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void notifyResult(String recognitionResult) {
        if (mCallback != null) {
            mCallback.onResult(recognitionResult);
        }
    }

    public interface Callback {
        void onResult(String recognitionResult);
    }

    public class ServiceHelperBinder extends Binder {
        ServiceHelper getService() {
            return ServiceHelper.this;
        }
    }
}
