package com.anysoftkeyboard.voice;

import com.anysoftkeyboard.utils.Log;

/**
 * Triggers a voice recognition by using {@link ImeTrigger} or
 * {@link IntentApiTrigger}.
 */
public class VoiceRecognitionTriggerV11 extends VoiceRecognitionTrigger {
    private static final String TAG = "ASK_VoiceRecognitionTriggerV11";

    public VoiceRecognitionTriggerV11(VoiceInputDiagram diagram) {
        super(diagram);
    }

    protected Trigger getTrigger() {
        if (ImeTrigger.isInstalled(mInputMethodService)) {
            return getImeTrigger();
        } else {
            Log.d(TAG, "ImeTrigger is not installed");
            return super.getTrigger();
        }
    }

    private Trigger getImeTrigger() {
        return new ImeTrigger(mInputMethodService);
    }
}
