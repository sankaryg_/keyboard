package com.anysoftkeyboard.voice;

/**
 * Triggers a voice recognition.
 */
interface Trigger {

    /**
     * Start a voice recognition.
     *
     * @param language The voice recognition language.
     */
    public abstract void startVoiceRecognition(String language);

    /**
     * Notifies that the calling IME started.
     */
    public abstract void onStartInputView();
}
