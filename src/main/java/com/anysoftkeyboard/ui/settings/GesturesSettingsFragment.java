
package com.anysoftkeyboard.ui.settings;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.preference.PreferenceFragment;

import com.hamburg.android.anysoftkeyboard.R;

import net.evendanan.pushingpixels.PassengerFragmentSupport;

public class GesturesSettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        addPreferencesFromResource(R.xml.prefs_gestures_prefs);
    }

    @Override
    public void onStart() {
        super.onStart();
	    PassengerFragmentSupport.setActivityTitle(this, getString(R.string.swipe_tweak_group));
    }
}
