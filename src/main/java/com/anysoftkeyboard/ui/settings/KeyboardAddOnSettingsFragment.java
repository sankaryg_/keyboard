package com.anysoftkeyboard.ui.settings;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceCategory;
import android.support.v4.preference.PreferenceFragment;

import com.anysoftkeyboard.keyboards.KeyboardAddOnAndBuilder;
import com.anysoftkeyboard.keyboards.KeyboardFactory;
import com.anysoftkeyboard.ui.settings.widget.AddOnCheckBoxPreference;
import com.hamburg.android.anysoftkeyboard.R;

import net.evendanan.pushingpixels.PassengerFragmentSupport;

import java.util.ArrayList;

public class KeyboardAddOnSettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        addPreferencesFromResource(R.xml.prefs_keyboards);
    }

    @Override
    public void onStart() {
        super.onStart();

        PreferenceCategory keyboardsGroup = (PreferenceCategory) findPreference("keyboard_addons_group");
        Activity activity = getActivity();
	    PassengerFragmentSupport.setActivityTitle(this, getString(R.string.keyboards_group));
        // getting all keyboards
        final ArrayList<KeyboardAddOnAndBuilder> creators = KeyboardFactory.getAllAvailableKeyboards(activity.getApplicationContext());

        keyboardsGroup.removeAll();

        for (final KeyboardAddOnAndBuilder creator : creators) {
            final AddOnCheckBoxPreference checkBox = new AddOnCheckBoxPreference(activity, null, R.style.Theme_AppCompat_Light);
            checkBox.setAddOn(creator);
            keyboardsGroup.addPreference(checkBox);
        }
    }
}
