package com.anysoftkeyboard.ui.settings;

import android.os.Bundle;
import android.support.v4.preference.PreferenceFragment;
import android.view.View;

import com.anysoftkeyboard.addons.AddOn;
import com.anysoftkeyboard.ui.settings.widget.AddOnListPreference;

import java.util.List;

public abstract class AbstractAddOnSelectorFragment<E extends AddOn> extends PreferenceFragment {

    private AddOnListPreference mAddOnsList;

    @Override
    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        addPreferencesFromResource(getPrefsLayoutResId());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAddOnsList = (AddOnListPreference) findPreference(getString(getAddOnsListPrefKeyResId()));
    }

    protected abstract AddOn getCurrentSelectedAddOn();

    @Override
    public void onStart() {
        super.onStart();
        final List<E> keys = getAllAvailableAddOns();
        AddOnListPreference.populateAddOnListPreference(mAddOnsList, keys, getCurrentSelectedAddOn());
    }

    protected abstract List<E> getAllAvailableAddOns();

    protected abstract int getPrefsLayoutResId();

    protected abstract int getAddOnsListPrefKeyResId();
}