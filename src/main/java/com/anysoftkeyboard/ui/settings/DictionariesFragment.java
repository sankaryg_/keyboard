

package com.anysoftkeyboard.ui.settings;

import android.app.Activity;
import android.os.Bundle;
import android.preference.Preference;
import android.support.v4.preference.PreferenceFragment;

import com.anysoftkeyboard.ui.settings.wordseditor.AbbreviationDictionaryEditorFragment;
import com.anysoftkeyboard.ui.settings.wordseditor.UserDictionaryEditorFragment;
import com.hamburg.android.anysoftkeyboard.R;

import net.evendanan.pushingpixels.FragmentChauffeurActivity;
import net.evendanan.pushingpixels.PassengerFragmentSupport;

public class DictionariesFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener {

    @Override
    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        addPreferencesFromResource(R.xml.prefs_dictionaries);
        findPreference(getString(R.string.user_dict_editor_key)).setOnPreferenceClickListener(this);
        findPreference(getString(R.string.abbreviation_dict_editor_key)).setOnPreferenceClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
	    PassengerFragmentSupport.setActivityTitle(this, getString(R.string.special_dictionaries_group));
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if (preference.getKey().equals(getString(R.string.user_dict_editor_key))) {
            Activity activity = getActivity();
            if (activity != null && activity instanceof FragmentChauffeurActivity) {
                ((FragmentChauffeurActivity)activity).addFragmentToUi(new UserDictionaryEditorFragment(), FragmentChauffeurActivity.FragmentUiContext.DeeperExperience);
                return true;
            }
        } else if (preference.getKey().equals(getString(R.string.abbreviation_dict_editor_key))) {
            Activity activity = getActivity();
            if (activity != null && activity instanceof FragmentChauffeurActivity) {
                ((FragmentChauffeurActivity)activity).addFragmentToUi(new AbbreviationDictionaryEditorFragment(), FragmentChauffeurActivity.FragmentUiContext.DeeperExperience);
                return true;
            }
        }
        return false;
    }
}
