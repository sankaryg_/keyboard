package com.anysoftkeyboard.ui.settings;

import android.os.Bundle;
import android.support.v4.preference.PreferenceFragment;

import com.hamburg.android.anysoftkeyboard.R;

public class KeyboardThemeTweaksFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        addPreferencesFromResource(R.xml.prefs_keyboard_theme_tweaks);
    }
}
