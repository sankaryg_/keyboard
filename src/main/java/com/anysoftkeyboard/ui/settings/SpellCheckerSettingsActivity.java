package com.anysoftkeyboard.ui.settings;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import com.hamburg.android.anysoftkeyboard.R;

/**
 * Spell checker preference screen.
 */
public class SpellCheckerSettingsActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.spell_checker_settings);
    }
}
