package com.anysoftkeyboard.ui.settings;

import com.anysoftkeyboard.addons.AddOn;
import com.anysoftkeyboard.keyboardextensions.KeyboardExtension;
import com.anysoftkeyboard.keyboardextensions.KeyboardExtensionFactory;
import com.hamburg.android.anysoftkeyboard.R;

import java.util.List;

public class BottomRowSelectorFragment extends AbstractAddOnSelectorFragment<KeyboardExtension> {

    @Override
    protected int getAddOnsListPrefKeyResId() {
        return R.string.settings_key_ext_kbd_bottom_row_key;
    }

    @Override
    protected int getPrefsLayoutResId() {
        return R.xml.prefs_bottom_row_addons;
    }

    @Override
    protected List<KeyboardExtension> getAllAvailableAddOns() {
        return KeyboardExtensionFactory.getAllAvailableExtensions(
                getActivity().getApplicationContext(),
                KeyboardExtension.TYPE_BOTTOM);
    }

    @Override
    protected AddOn getCurrentSelectedAddOn() {
        return KeyboardExtensionFactory.getCurrentKeyboardExtension(
                getActivity().getApplicationContext(),
                KeyboardExtension.TYPE_BOTTOM);
    }
}