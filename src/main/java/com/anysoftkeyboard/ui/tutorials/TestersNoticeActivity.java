
package com.anysoftkeyboard.ui.tutorials;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.hamburg.android.anysoftkeyboard.R;

public class TestersNoticeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testers_version);
    }

    public void onCloseClicked(View v) {
        finish();
    }
}
