

package com.anysoftkeyboard.addons;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.support.annotation.Nullable;

import com.anysoftkeyboard.utils.Log;

import java.lang.ref.WeakReference;

public abstract class AddOnImpl implements AddOn {

	private static final String TAG = "ASK_AddOnImpl";
	private final String mId;
    private final String mName;
    private final String mDescription;
    private final String mPackageName;
    private final Context mAskAppContext;
    private WeakReference<Context> mPackageContext;
    private final int mSortIndex;

    protected AddOnImpl(Context askContext, Context packageContext, String id, int nameResId,
                        String description, int sortIndex) {
        mId = id;
        mAskAppContext = askContext;
        mName = packageContext.getString(nameResId);
        mDescription = description;
        mPackageName = packageContext.getPackageName();
        mPackageContext = new WeakReference<>(packageContext);
        mSortIndex = sortIndex;
    }

    public final String getId() {
        return mId;
    }

    public final String getDescription() {
        return mDescription;
    }

    public String getPackageName() {
        return mPackageName;
    }

    @Nullable
    public final Context getPackageContext() {
        Context c = mPackageContext.get();
        if (c == null) {
            try {
                c = mAskAppContext.createPackageContext(mPackageName, PackageManager.GET_META_DATA);
                mPackageContext = new WeakReference<>(c);
            } catch (NameNotFoundException e) {
	            Log.w(TAG, "Failed to find package %s!", mPackageName);
	            Log.w(TAG, "Failed to find package! ", e);
            }
        }
        return c;
    }

    public final int getSortIndex() {
        return mSortIndex;
    }

    public String getName() {
        return mName;
    }
}
