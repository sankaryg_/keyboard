

package com.anysoftkeyboard.addons;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;

public interface IconHolder {
    @Nullable
    Drawable getIcon();
}
