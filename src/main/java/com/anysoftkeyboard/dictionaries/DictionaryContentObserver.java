package com.anysoftkeyboard.dictionaries;

import android.database.ContentObserver;
import com.anysoftkeyboard.utils.Log;
import net.evendanan.frankenrobot.Diagram;

import java.lang.ref.WeakReference;

/**
 * User: hamburg
 * Date: 3/10/13
 * Time: 11:48 PM
 */
public class DictionaryContentObserver extends ContentObserver {

    public static final class DictionaryContentObserverDiagram extends Diagram<DictionaryContentObserver> {
        private final BTreeDictionary mOwningDictionary;
        public DictionaryContentObserverDiagram(BTreeDictionary owningDictionary) {
            mOwningDictionary = owningDictionary;
        }

        public BTreeDictionary getOwningDictionary() {
            return mOwningDictionary;
        }
    }

    private final static String TAG = "DictionaryContentObserver";
    private final WeakReference<BTreeDictionary> mDictionary;

    public DictionaryContentObserver(DictionaryContentObserverDiagram diagram) {
        super(null);
        mDictionary = new WeakReference<BTreeDictionary>(diagram.getOwningDictionary());
    }

    @Override
    public void onChange(boolean self) {
        BTreeDictionary dictionary = mDictionary.get();
        if (dictionary == null) return;
        if (self) {
            Log.i(TAG, "I wont notify about self change.");
            return;
        }

        dictionary.onStorageChanged();
    }
}
