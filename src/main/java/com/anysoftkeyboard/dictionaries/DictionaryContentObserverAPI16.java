package com.anysoftkeyboard.dictionaries;

import android.net.Uri;

/**
 * User: hamburg
 *  */
public class DictionaryContentObserverAPI16 extends DictionaryContentObserver {
    public DictionaryContentObserverAPI16(DictionaryContentObserverDiagram diagram) {
        super(diagram);
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange);
    }
}
